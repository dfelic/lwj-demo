import React, { Component } from 'react';

import './Reviews.css';

import Review from '../../components/Review/Review';

import student1 from '../../assets/img/student1.jpg';
import student2 from '../../assets/img/student2.png';
class Reviews extends Component {
  render() {
    return (
      <section className="section-reviews" id="reviews">
        <div className={'util-center-text util-margin-bottom '}>
          <h2 className="heading-secondary">Helping you do your very best!</h2>
        </div>
        <Review
          studentPic={student1}
          studentName="Mary Smith SAT:1432"
          quoteAuthor="Parent of Ally"
        />
        <Review studentPic={student2} studentName="Jack Wilson" />

        <div className="util-center-text">
          <a href="#" className="btn-text">
            Read all stories &rarr;
          </a>
        </div>
      </section>
    );
  }
}

export default Reviews;
