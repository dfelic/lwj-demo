import React from 'react';

import './Service.css';

const Service = props => (
  <div className="card">
    <div className="card__side card__side--front">
      <div className="card__picture card__picture--1">&nbsp;</div>
      <h4 className="card__heading">
        <span className="card__heading-span card__heading-span--1">
          1-On 1 In Person
        </span>
      </h4>
      <div className="card__details">
        <ul>
          <li>This is</li>
          <li>Filler information</li>
          <li>If you finish</li>
          <li>reading this you're</li>
          <li>Difficulty: easy</li>
        </ul>
      </div>
    </div>
    <div className="card__side card__side--back card__side--back-1">
      <div className="card__cta">
        <div className="card__price-box">
          <p className="card__price-only">Only</p>
          <p className="card__price-value">$20/hr</p>
        </div>
        <a href="#popup" className="btn btn--white">
          Book now!
        </a>
      </div>
    </div>
  </div>
);

export default Service;
