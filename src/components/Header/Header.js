import React from 'react';

import './Header.css';

const Header = props => (
  <header className="header">
    <div className="logo" />
    {/* <div className="header-img" /> */}
    <div className="top-clip" />
  </header>
);

export default Header;
