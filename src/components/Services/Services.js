import React, { Component } from 'react';

import './Services.css';

import Service from '../Service/Service';

class Services extends Component {
  render() {
    return (
      <section className="section-services" id="services">
        <div className="util-center-text util-margin-bottom-big util-margin-top-big">
          <h2 className="heading-secondary">
            Explore your options for success!
          </h2>
        </div>
        <div className="services">
          <div className="service-container">
            <Service />
          </div>
          <div className="service-container">
            <Service />
          </div>
          <div className="service-container">
            <Service />
          </div>
        </div>
      </section>
    );
  }
}

export default Services;
