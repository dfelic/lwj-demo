import React from 'react';

import './Button.css';

const Button = props => (
  <button className={props.styleName}>
    <strong>{props.children}</strong>
  </button>
);
export default Button;
