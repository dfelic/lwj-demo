import React from 'react';

import './Review.css';

const Review = props => (
  <div className="story">
    <figure className="story__shape">
      <img
        src={props.studentPic}
        alt="Person on a tour"
        className="story__img"
      />
      <figcaption className="story__caption">{props.studentName}</figcaption>
    </figure>
    <div className="story__text">
      <h3 className="heading-tertiary u-margin-bottom-small">
        "... I wouldn't hesistate to use her again."
      </h3>
      <blockquote>
        “Jasmin was very helpful in getting my daughter ready for the SAT,
        covering not just the possible subject matter but test taking strategy
        and time management while taking the test. She was organized and engaged
        in the subject matter and I wouldn’t hesitate to use her again.”
        <h4>
          <i>-{props.quoteAuthor}</i>
        </h4>
      </blockquote>
    </div>
  </div>
);

export default Review;
