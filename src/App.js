import React, { Component } from 'react';

import './App.css';

import Reviews from './containers/Reviews/Reviews';
import Header from './components/Header/Header';
import Services from './components/Services/Services';
import Button from './components/Button/Button';

class App extends Component {
  state = {
    stickyNav: false
  };
  // to go in Navigation component.
  componentDidMount() {
    window.addEventListener('scroll', this.onScrollHandler);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollHandler);
  }

  // onScrollHandler = () => {
  //   var scroll = window.pageYOffset;
  //   // console.log(scroll);
  //   if (scroll > 425) {
  //     console.log('bop');
  //   } else {
  //   }
  // };
  onScrollHandler = () => {
    console.log(window.pageYOffset);
    if (this.state.stickyNav === false) {
      if (window.pageYOffset > 429.99) {
        this.setState({ stickyNav: true });
      }
    } else {
      if (window.pageYOffset < 430) {
        this.setState({ stickyNav: false });
      }
    }
  };
  render() {
    return (
      <div className="app">
        <Header />
        <div
          className={
            this.state.stickyNav ? 'content util-margin-top-big' : 'content'
          }
        >
          {/*<Navigation>  ? </Navigation> */}

          <nav
            className={!this.state.stickyNav ? 'header-nav' : 'header-nav-alt'}
          >
            <a href="#booking">
              <Button styleName="btn btn--green">Book A Session</Button>
            </a>
            <a href="#services">
              <Button styleName="btn btn--green">Services</Button>
            </a>
            <a href="#reviews">
              <Button styleName="btn btn--green">Reviews</Button>
            </a>
          </nav>

          <Reviews />
          <Services />
          <section className="booking" id="booking">
            <section className="section-book">
              <div className="row">
                <div className="book">
                  <div className="book__form">
                    <form action="#" className="form">
                      <div className="u-margin-bottom-medium">
                        <h2 className="heading-secondary">Start booking now</h2>
                      </div>

                      <div className="form__group">
                        <input
                          type="text"
                          className="form__input"
                          placeholder="Full name"
                          id="name"
                          required
                        />
                        <label /*for="name"*/ className="form__label">
                          Full name
                        </label>
                      </div>

                      <div className="form__group">
                        <input
                          type="email"
                          className="form__input"
                          placeholder="Email address"
                          id="email"
                          required
                        />
                        <label /*for="email"*/ className="form__label">
                          Email address
                        </label>
                      </div>

                      <div className="form__group u-margin-bottom-medium">
                        <div className="form__radio-group">
                          <input
                            type="radio"
                            className="form__radio-input"
                            id="small"
                            name="size"
                          />
                          <label /*for="small"*/ className="form__radio-label">
                            <span className="form__radio-button" />
                            In Person 1-On-1
                          </label>
                        </div>
                        <div className="form__radio-group">
                          <input
                            type="radio"
                            className="form__radio-input"
                            id="small"
                            name="size"
                          />
                          <label /*for="small"*/ className="form__radio-label">
                            <span className="form__radio-button" />
                            In Person Group
                          </label>
                        </div>
                      </div>

                      <div className="form__group u-margin-bottom-medium">
                        <div className="form__radio-group">
                          <input
                            type="radio"
                            className="form__radio-input"
                            id="large"
                            name="size"
                          />
                          <label /*or="large"*/ className="form__radio-label">
                            <span className="form__radio-button" />
                            Online Lessons
                          </label>
                        </div>
                      </div>

                      <div className="form__group">
                        <button className="btn btn--green">
                          Next step &rarr;
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          </section>

          <footer>Hi</footer>
        </div>
      </div>
    );
  }
}

export default App;
